from setuptools import setup


setup(
    name='pytvrage',
    version='0.1',
    author='David Fernandez',
    author_email='david@davidfm.es',
    packages=[
        'pytvrage',
    ],
    license='LICENSE.txt',
    description='A Python interface for the TVRage XML API',
    long_description=open('README.rst').read(),
    classifiers=[
        'Development Status :: 1 - Planning',
        'Programming Language :: Python :: 2.7',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)'
    ]


)